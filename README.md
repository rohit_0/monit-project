# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Using monit tool to monitor services and restart them if they fail.

### How do I get set up? ###

Install and configure Monit on Ubuntu from here:

https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-monit

Locate your monitrc file in /etc/monit/monitrc and replace its content with the content in the uploaded monitrc file. (You will need to do sudo bash to access this)

Set up Apache2, MySQL and MongoDB servers on your machine.

Commands for using monit:

Initialising monit: 		monit
Reloading the monitrc file: monit reload          ;; Do this after every change you make to the file.
Displaying current status:  monit status

To test if monit works:

Stop a service manually and check if monit restarts it automatically.

sudo service <service name> stop

Go to http://localhost:2812/_viewlog? and view the log file for work done by monit.

To check if service has been restarted either check the logs or do : service <service name> status

If it returns active, the service has been restarted successfully.


### Who do I talk to? ###

In case of any doubts contact Rohit Tapas.